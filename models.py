from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    contacts = db.relationship('UserContact')


class UserContact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    contact = db.Column(db.String(255), nullable=False)
    tags = db.relationship('UserContactTag')
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)


class UserContactTag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(255), nullable=False)
    user_contact_id = db.Column(db.Integer, db.ForeignKey("user_contact.id"), nullable=False)


FROM python:3.7-alpine

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip3 install --no-cache -r requirements.txt

COPY . .

EXPOSE 5000

CMD ["python3", "-u", "app.py"]

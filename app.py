from flask import Flask, request, jsonify
from flask_marshmallow import Marshmallow
from marshmallow import fields

from models import db, User, UserContact, UserContactTag
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"

db.app = app
db.init_app(app)
db.create_all()
ma = Marshmallow(app)


class UserContactTagSchema(ma.ModelSchema):
    class Meta:
        model = UserContactTag


class UserContactSchema(ma.ModelSchema):
    tags = fields.Nested(UserContactTagSchema, many=True)
    class Meta:
        model = UserContact


class UserSchema(ma.ModelSchema):
    contacts = fields.Nested(UserContactSchema, many=True)
    class Meta:
        model = User


@app.route('/users', methods=['POST'])
def add_user():
    json = request.get_json() or None
    if json:
        schema = UserSchema()
        data, errors = schema.load(json)
        if errors:
            return jsonify(errors=errors), 400

        db.session.add(data)
        db.session.commit()
        user = schema.dump(data).data
        return jsonify(user=user), 200
    return 400


@app.route('/users', methods=['GET'])
def get_users():
    users = User.query.all()
    data = UserSchema(many=True).dump(users).data
    return jsonify(users=data), 200


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
